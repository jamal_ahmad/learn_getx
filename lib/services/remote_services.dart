import 'package:cakeandpastries_ui/Model/product.dart';
import 'package:http/http.dart' as http;

class RemoteServices {
  static var client = http.Client();

  static Future<List<Product>> fetchProducts() async {
    var url =
        "https://makeup-api.herokuapp.com/api/v1/products.json?brand=maybelline";
    var response = await client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var jsonString = response.body;
      return productFromJson(jsonString);
    } else {
      //show error here
      return null;
    }
  }
}
