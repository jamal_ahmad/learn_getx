import 'package:cakeandpastries_ui/Model/product.dart';
import 'package:cakeandpastries_ui/services/remote_services.dart';
import 'package:get/state_manager.dart';

class ProductController extends GetxController {
  var productList = <Product>[].obs;

  @override
  void onInit() {
    fetchProducts();
    super.onInit();
  }

  void fetchProducts() async {
    var products = await RemoteServices.fetchProducts();
    if (products != null) {
      productList.value = products;
    }
  }
}
